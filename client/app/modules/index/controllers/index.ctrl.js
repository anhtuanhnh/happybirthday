'use strict';
angular
  .module('com.module.index')
  .controller('index', function ($scope) {
    $scope.number = [{label: 1}, {label: 2}, {label: 3}, {label: 4}, {label: 5}, {label: 6}, {label: 7}, {label: 8}];
    $scope.slickConfig = {
      autoplay: true,
      infinite: true,
      autoplaySpeed: 3000,
      slidesToShow: 3,
      slidesToScroll: 1,
      method: {}
    };

    var logo_rotate = $("header .wed_logo_animation").attr('data-rotate');
    if (logo_rotate!='') {
      $("header .wed_logo_animation").addClass('wed_logo_rotate_'+logo_rotate);
    }

    // $("header .wed_logo_animation").lettering();
    $("header .wed_logo_animation span").each(function(){
      console.log(1);
      var min = 0;
      var max = 50;
      var randomNumber = Math.floor(Math.random()*(max-min+1)+min);
      $(this).css('transition-delay', '0.'+randomNumber+'s');
    });

    var i = 1;
    var y = -16.8;

    $(window).scroll(function(){
      i = (window.innerHeight- $(window).scrollTop())/window.innerHeight;
      y = -16.8 -(150 -100*i);
      $(".skrollable-between").css('opacity', i);
      $(".wed_slider").css('background-position', "0%"+ y+"px");

      if ($(window).scrollTop() > 100) {
        $(".wed_logo").addClass('active');
        $('body').addClass('wed_first_step');

      }
      else {
        $('body').removeClass('wed_first_step');
        $(".wed_logo").removeClass('active');
      }
      if ($(window).scrollTop() > 500) {
        $('body').addClass('wed_second_step');
      }
      else {
        $('body').removeClass('wed_second_step');
      }
      if ($(window).scrollTop() > 1520) {
        $('.wed_section_inner_animal_1').addClass('active');
        $('.wed_section_inner_animal_1').removeClass('noactive');

      }
      else {
        $('.wed_section_inner_animal_1').addClass('noactive');
        $('.wed_section_inner_animal_1').removeClass('active');
      }
      if ($(window).scrollTop() > 2000) {
        $('.wed_section_inner_animal_2').addClass('active');
        $('.wed_section_inner_animal_2').removeClass('noactive');

      }
      else {
        $('.wed_section_inner_animal_2').addClass('noactive');
        $('.wed_section_inner_animal_2').removeClass('active');
      }
      if ($(window).scrollTop() > 2500) {
        $('.wed_section_inner_animal_3').addClass('active');
        $('.wed_section_inner_animal_3').removeClass('noactive');

      }
      else {
        $('.wed_section_inner_animal_3').addClass('noactive');
        $('.wed_section_inner_animal_3').removeClass('active');
      }
      if ($(window).scrollTop() > 3100) {
        $('.wed_section_inner_animal_4').addClass('active');
        $('.wed_section_inner_animal_4').removeClass('noactive');

      }
      else {
        $('.wed_section_inner_animal_4').addClass('noactive');
        $('.wed_section_inner_animal_4').removeClass('active');
      }
      if ($(window).scrollTop() > 3700) {
        $('.wed_section_inner_animal_5').addClass('active');
        $('.wed_section_inner_animal_5').removeClass('noactive');

      }
      else {
        $('.wed_section_inner_animal_5').addClass('noactive');
        $('.wed_section_inner_animal_5').removeClass('active');
      }
      if ($(window).scrollTop() > 4200) {
        $('.wed_section_inner_animal_6').addClass('active');
        $('.wed_section_inner_animal_6').removeClass('noactive');

      }
      else {
        $('.wed_section_inner_animal_6').addClass('noactive');
        $('.wed_section_inner_animal_6').removeClass('active');
      }
    });

    $scope.showMusic = function () {
      $(".wed_iframe").toggleClass('active');
    }
  });


