'use strict';
angular
  .module('com.module.index')
  .config(function($stateProvider,$locationProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider
      	.state('home', {
        	url: '/',
	        views:{
	          '':{
	            templateUrl :'modules/index/views/home.html',
              controller: 'index'
	          },
            'home_header@home': {
              templateUrl: 'modules/index/views/home_header.html',
              controller: 'index'
            },
            'home_background@home': {
              templateUrl: 'modules/index/views/home_background.html'
            },
            'home_slide@home': {
              templateUrl: 'modules/index/views/home_slide.html'
            },
            'home_content@home': {
              templateUrl: 'modules/index/views/home_content.html'
            },
            'home_footer@home': {
              templateUrl: 'modules/index/views/home_footer.html'
            }
        }});
  });
