(function () {
  'use strict';
  angular
    .module('com.module.monitors')
    .service('MonitorsService', function (CoreService, Monitor, gettextCatalog) {
      this.getMonitors = function (userId) {
        console.log(userId);
        return Monitor.find({
          filter: {
            order: 'created DESC',
            where:{
              userId: userId
            }
          }

        }).$promise;
      };

      this.getMonitor = function (id) {
        return Monitor.findById({
          id: id
        }).$promise;
      };

      this.upsertMonitor = function (monitor) {
        return Monitor.upsert(monitor).$promise
          .then(function () {
            CoreService.toastSuccess(
              gettextCatalog.getString('Monitor saved'),
              gettextCatalog.getString('Your monitor is safe with us!')
            );
          })
          .catch(function (err) {
            CoreService.toastSuccess(
              gettextCatalog.getString('Error saving monitor '),
              gettextCatalog.getString('This monitor could no be saved: ') + err
            );
          }
        );
      };

      this.deleteMonitor = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            Monitor.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Monitor deleted'),
                gettextCatalog.getString('Your monitor is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting monitor'),
                gettextCatalog.getString('Your monitor is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };

      this.getFormFields = function () {
        return [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Name'),
              required: true
            }
          },
          {
            key: 'type',
            type: 'textarea',
            templateOptions: {
              label: gettextCatalog.getString('Surface Water Type')
            }
          },{
            key: 'note',
            type: 'textarea',
            templateOptions: {
              label: gettextCatalog.getString('Note')
            }
          },
          {
            key: 'lat',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Lat')
            }
          },{
            key: 'lng',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Lng')
            }
          },{
            key: 'ph',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Ph')
            }
          },{
            key: 'do',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Do')
            }
          },{
            key: 'no2',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('No2')
            }
          },{
            key: 'po4',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('PO4')
            }
          },{
            key: 'nh4',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('NH4')
            }
          },{
            key: 'tss',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('TSS')
            }
          },{
            key: 'bod5',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('BOD5')
            }
          },{
            key: 'cod',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('COD')
            }
          },{
            key: 'nhietdo',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Nhiet do')
            }
          },{
            key: 'doduc',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Do duc')
            }
          },{
            key: 'coliform',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('coliform')
            }
          },{
            key: 'qcvn',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('QCVN')
            }
          },{
            key: 'wqi',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('WQI')
            }
          }
        ];
      };

    });

})();
