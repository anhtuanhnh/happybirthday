(function () {
  'use strict';
  angular
    .module('com.module.monitors')
    .run(function ($rootScope, Monitor, gettextCatalog) {
      $rootScope.addMenu(gettextCatalog.getString('Monitors'), 'app.monitors.list', 'fa-edit');

      Monitor.find(function (monitors) {
        $rootScope.addDashboardBox(gettextCatalog.getString('Monitors'), 'bg-red', 'ion-document-text', monitors.length, 'app.monitors.list');
      });

    });

})();
