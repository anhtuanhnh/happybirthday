(function () {
  'use strict';
  angular
    .module('com.module.monitors')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.monitors', {
          abstract: true,
          url: '/monitors',
          templateUrl: 'modules/monitors/views/main.html'
        })
        .state('app.monitors.list', {
          url: '',
          templateUrl: 'modules/monitors/views/list.html',
          controllerAs: 'ctrl',
          controller: function (monitors,$rootScope) {
            this.monitors = monitors;
          },
          resolve: {
            monitors: [
              'MonitorsService','$rootScope',
              function (MonitorsService,$rootScope) {
                return MonitorsService.getMonitors();
              }
            ]
          }
        })
        .state('app.monitors.add', {
          url: '/add',
          templateUrl: 'modules/monitors/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, MonitorsService, monitor) {
            this.monitor = monitor;
            this.formFields = MonitorsService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              MonitorsService.upsertMonitor(this.monitor).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            monitor: function () {
              return {};
            }
          }
        })
        .state('app.monitors.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/monitors/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, MonitorsService, monitor) {
            console.log(monitor);
            this.monitor = monitor;
            this.formFields = MonitorsService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              MonitorsService.upsertMonitor(this.monitor).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            monitor: function ($stateParams, MonitorsService) {
              return MonitorsService.getMonitor($stateParams.id);
            }
          }
        })
        .state('app.monitors.view', {
          url: '/:id',
          templateUrl: 'modules/monitors/views/view.html',
          controllerAs: 'ctrl',
          controller: function (monitor) {
            this.monitor = monitor;
          },
          resolve: {
            monitor: function ($stateParams, MonitorsService) {
              return MonitorsService.getMonitor($stateParams.id);
            }
          }
        })
        .state('app.monitors.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, MonitorsService, monitor) {
            MonitorsService.deleteMonitor(monitor.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            monitor: function ($stateParams, MonitorsService) {
              return MonitorsService.getMonitor($stateParams.id);
            }
          }
        });
    }
  );

})();
